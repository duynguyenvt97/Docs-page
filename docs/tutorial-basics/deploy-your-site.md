---
sidebar_position: 5
---

# Triển khai trang web của bạn

Docusaurus là một **static-site-generator** (còn được gọi là **[Jamstack](https://jamstack.org/)**).

Nó xây dựng trang web của bạn dưới dạng các tệp **HTML, JavaScript và CSS tĩnh** đơn giản.

## Xây dựng trang web của bạn

Xây dựng trang web của bạn **cho sản xuất**:

```bash
npm run build
```

Các tệp tĩnh được tạo ra trong thư mục `build`.

## Triển khai trang web của bạn

Thử nghiệm bản build sản xuất của bạn tại địa phương:

```bash
npm run serve
```

Thư mục `build` giờ đây được phục vụ tại [http://localhost:3000/](http://localhost:3000/).

Bây giờ bạn có thể triển khai thư mục `build` **gần như bất cứ đâu** một cách dễ dàng, **miễn phí** hoặc với chi phí rất nhỏ (đọc **[Hướng dẫn Triển khai](https://docusaurus.io/docs/deployment)**).
