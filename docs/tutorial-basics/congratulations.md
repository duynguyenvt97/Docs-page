---
sidebar_position: 6
---

# Chúc mừng!

Bạn vừa học được **cơ bản về Docusaurus** và đã thực hiện một số thay đổi trong **mẫu ban đầu**.

Docusaurus còn **nhiều điều thú vị hơn**!

Có **5 phút nữa** không? Hãy xem qua phần **[quản lý phiên bản](../tutorial-extras/manage-docs-versions.md)** và **[i18n](../tutorial-extras/translate-your-site.md)**.

Có điều gì **không rõ** hoặc **có lỗi** trong hướng dẫn này không? [Xin báo cáo nó!](https://github.com/facebook/docusaurus/discussions/4610)

## Tiếp theo là gì?

- Đọc [tài liệu chính thức](https://docusaurus.io/)
- Sửa đổi cấu hình trang web của bạn với [`docusaurus.config.js`](https://docusaurus.io/docs/api/docusaurus-config)
- Thêm mục navbar và footer với [`themeConfig`](https://docusaurus.io/docs/api/themes/configuration)
- Thêm một [Thiết kế và Bố cục](https://docusaurus.io/docs/styling-layout) tùy chỉnh
- Thêm một [thanh tìm kiếm](https://docusaurus.io/docs/search)
- Tìm cảm hứng trong [Bảng giới thiệu Docusaurus](https://docusaurus.io/showcase)
- Tham gia vào [Cộng đồng Docusaurus](https://docusaurus.io/community/support)
