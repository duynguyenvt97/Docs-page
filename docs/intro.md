---
sidebar_position: 1
---

# Giới thiệu Hướng dẫn

Hãy khám phá **Docusaurus trong ít hơn 5 phút**.

## Bắt đầu

Bắt đầu bằng cách **tạo một trang web mới**.

Hoặc **thử Docusaurus ngay lập tức** với **[docusaurus.new](https://docusaurus.new)**.

### Những gì bạn sẽ cần

- [Node.js](https://nodejs.org/en/download/) phiên bản 18.0 trở lên:
  - Khi cài đặt Node.js, bạn được khuyến khích kiểm tra tất cả các ô liên quan đến phụ thuộc.

## Tạo một trang web mới

Tạo một trang web Docusaurus mới sử dụng **mẫu cổ điển**.

Mẫu cổ điển sẽ tự động được thêm vào dự án của bạn sau khi bạn chạy lệnh:

```bash
npm init docusaurus@latest my-website classic
```

Bạn có thể nhập lệnh này vào Command Prompt, Powershell, Terminal, hoặc bất kỳ terminal tích hợp nào của trình soạn thảo mã của bạn.

Lệnh này cũng cài đặt tất cả các phụ thuộc cần thiết bạn cần để chạy Docusaurus.

## Khởi động trang web của bạn

Chạy máy chủ phát triển:

```bash
cd my-website
npm run start
```

Lệnh `cd` thay đổi thư mục bạn đang làm việc. Để làm việc với trang web Docusaurus mới tạo của bạn, bạn sẽ cần điều hướng terminal đến đó.

Lệnh `npm run start` xây dựng trang web của bạn nội bộ và phục vụ nó thông qua một máy chủ phát triển, sẵn sàng cho bạn để xem tại

Mở `docs/intro.md` (trang này) và chỉnh sửa một số dòng: trang web sẽ **tự động tải lại** và hiển thị các thay đổi của bạn.